﻿using Newtonsoft.Json;
using ParkingLib;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace Parking
{
    class Menu
    {
        private bool run = true;
        HttpClient httpClient;

        public Menu()
        {
            httpClient = new HttpClient();
            Console.SetWindowSize(100, 30);
        }

        public void MainMenu()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Weclome to Park Station simulator. \n" +
                "Please, press any key to continue.");
            Console.ResetColor();

            while (run)
            {
                ShowMenu();
                GetInput();
            }
        }

        private void ShowMenu()
        {
            Console.ReadKey();
            Console.Clear();
            var currentColor = Console.ForegroundColor;
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Please choose an option below");
            Console.ForegroundColor = currentColor;

            Console.WriteLine("1. Show vehicles on parking \n" +
                "2. Add vehicle to parking \n" +
                "3. Remove vehicle from parking \n" +
                "4. Show park balance \n" +
                "5. Fill vehicle balance \n" +
                "6. Find vehicle by id \n" +
                "7. Show free spaces \n" +
                "8. Show transactions for last minute \n" +
                "9. Show all transactions \n" +
                "10. Show profit for last minute. \n" +
                "0 (Enter). Exit \n");
        }

        private void GetInput()
        {
            int.TryParse(Console.ReadLine(), out int responce);

            Console.Clear();
            switch (responce)
            {
                case 0:
                    run = false;
                    break;
                case 1:
                    ShowAllVehicle();
                    break;
                case 2:
                    AddVehicle();
                    break;
                case 3:
                    RemoveVehicle();
                    break;
                case 4:
                    GetParkBalance();
                    break;
                case 5:
                    AddBalance();
                    break;
                case 6:
                    GetVehicle();
                    break;
                case 7:
                    GetFreeSpaces();
                    break;
                case 8:
                    GetTransactionForLastMinute();
                    break;
                case 9:
                    GetLogs();
                    break;
                case 10:
                    GetProfitPerMinute();
                    break;
                default:
                    DisplayError($"There are no option like {responce}.");
                    ShowMenu();
                    break;
            }
        }
        private void ShowAllVehicle()
        {
            try
            {
                var responce = httpClient.GetStringAsync("https://localhost:5001/api/vehicle");
                List<Vehicle> list = JsonConvert.DeserializeObject<List<Vehicle>>(responce.Result);

                foreach (var vehicle in list)
                {
                    Console.WriteLine(vehicle);
                }
            }
            catch (NullReferenceException)
            {
                DisplayError("There are no vehicle on parking station.");
            }

            PauseToContinue();
        }

        private void AddVehicle()
        {
            Console.WriteLine("Please, select vehicle type: \n" +
                "1. Car \n" +
                "2. Truck \n" +
                "3. Bus \n" +
                "4. Motorcycle \n");
            string respond = Console.ReadLine();

            try
            {
                int vehicleType = Convert.ToInt32(respond);
                if (vehicleType > 0 && vehicleType < 5)
                {
                    Console.WriteLine("Please, add some balance to this vehicle.");
                    decimal.TryParse(Console.ReadLine(), out decimal vehicleBalance);

                    if (vehicleBalance > 0)
                    {
                        Vehicle vehicle = new Vehicle(vehicleBalance, (VehicleType)vehicleType);
                        vehicle.Id = ++Vehicle.id;
                        var jsonvehicle = JsonConvert.SerializeObject(vehicle);
                        var content = new StringContent(jsonvehicle, Encoding.UTF8, "application/json");

                        var responce = httpClient.PostAsync("https://localhost:5001/api/vehicle", content);
                        if (responce.Result.StatusCode == System.Net.HttpStatusCode.NotFound)
                        {
                            DisplayError("Can't add vehicle. All slots are occupied.");
                        }
                        else
                        {
                            Console.WriteLine("Vehicle was added successfully.");
                        }
                    }
                    else
                    {
                        DisplayError("Wrong balance, please try again.");
                    }
                }
                else
                {
                    DisplayError("Wrong vehicle type, please try again.");
                }
            }
            catch (HttpRequestException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (FormatException)
            {
                DisplayError("Wrong format. Please try again.");
            }

            PauseToContinue();
        }
        private void RemoveVehicle()
        {
            Console.WriteLine("Please, enter vehicle id to remove.");
            int.TryParse(Console.ReadLine(), out int id);

            if (id <= 0)
            {
                DisplayError("Wrong id. Please try again.");
                PauseToContinue();
                return;
            }

            var responce = httpClient.DeleteAsync("https://localhost:5001/api/vehicle/" + id);
            if (responce.Result.StatusCode == System.Net.HttpStatusCode.NotFound)
            {
                DisplayError($"Vehicle with id {id} was not found or it has unpaid fine.");
            }

            PauseToContinue();
        }

        private void GetParkBalance()
        {
            var responce = httpClient.GetStringAsync("https://localhost:5001/api/parking/balance");
            Console.WriteLine($"Current balance: {responce.Result}");
            PauseToContinue();
        }

        private void AddBalance()
        {
            try
            {
                Console.WriteLine("Please, enter vehicle id.");
                int id = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Please, enter the amount you want to put on vehicle balance.");
                decimal.TryParse(Console.ReadLine(), out decimal vehicleBalance);
                if (vehicleBalance > 0)
                {
                    var content = new StringContent(vehicleBalance.ToString(), Encoding.UTF8, "application/json");
                    var responce = httpClient.PostAsync($"https://localhost:5001/api/vehicle/fill/{id}", content);
                    if (responce.Result.StatusCode == System.Net.HttpStatusCode.NotFound)
                    {
                        DisplayError("Vehicle not found.");
                    }
                    else
                    {
                        Console.WriteLine("Balance was filled successfully.");
                    }
                }
                else
                {
                    DisplayError("Wrong format. Balance can't be string, zero or negative number.");
                }
            }
            catch (NullReferenceException)
            {
                DisplayError("Cant find vehicle. Please try again");
            }
            catch (FormatException)
            {
                DisplayError("Wrong format. Please try again.");
            }
            PauseToContinue();
        }
        private void GetVehicle()
        {
            try
            {
                Console.WriteLine("Please, enter vehicle id.");
                int id = Convert.ToInt32(Console.ReadLine());
                var responce = httpClient.GetStringAsync($"https://localhost:5001/api/vehicle/{id}");
                if (responce.Result.Length > 0)
                {
                    Vehicle vehicle = JsonConvert.DeserializeObject<Vehicle>(responce.Result);
                    Console.WriteLine(vehicle);

                }
                else
                {
                    DisplayError("Vehicle was not found. Please try another id.");
                }
            }
            catch (AggregateException)
            {
                DisplayError("Vehicle was not found. Please try another id.");
            }
            catch (FormatException)
            {
                DisplayError("Wrong format. Please try again.");
            }
            PauseToContinue();
        }

        private void GetFreeSpaces()
        {
            var responce = httpClient.GetStringAsync("https://localhost:5001/api/parking/freespaces");
            if (Convert.ToInt32(responce.Result) > 0)
            {
                Console.WriteLine($"There are {responce.Result} slots avaliable now.");
            }
            else
            {
                Console.WriteLine("There are no free slots on parking.");
            }
            PauseToContinue();
        }

        private void GetTransactionForLastMinute()
        {
            var responce = httpClient.GetStringAsync("https://localhost:5001/api/transactions/last");
            List<Transaction> transactions = JsonConvert.DeserializeObject<List<Transaction>>(responce.Result);
            if (transactions.Count < 1)
            {
                Console.WriteLine("There are no transactions yet.");
            }
            else
            {
                foreach (var transaction in transactions)
                {
                    Console.WriteLine(transaction.ToString());
                }
            }
            PauseToContinue();
        }

        private void GetLogs()
        {
            var responce = httpClient.GetStringAsync("https://localhost:5001/api/transactions/");
            List<Transaction> transactions = JsonConvert.DeserializeObject<List<Transaction>>(responce.Result);

            foreach (var transaction in transactions)
            {
                Console.WriteLine(transaction.ToString());
            }
            PauseToContinue();
        }

        private void GetProfitPerMinute()
        {
            var responce = httpClient.GetStringAsync("https://localhost:5001/api/parking/profit");
            Console.WriteLine($"Total profit per last minute: {responce.Result}.");
            PauseToContinue();
        }

        private void PauseToContinue()
        {
            Console.WriteLine("\nPress any key to continue.");
        }

        private void DisplayError(string message)
        {
            var current = Console.ForegroundColor;
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write($"ERROR: ");
            Console.ForegroundColor = current;
            Console.WriteLine(message);
        }
    }
}
