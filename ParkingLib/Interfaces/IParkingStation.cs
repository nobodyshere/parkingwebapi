﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ParkingLib.Interfaces
{
    public interface IParkingStation
    {
        decimal GetProfitForLastMinute();
        List<Transaction> GetTransactionsPerMinute();
        bool AddVehicle(Vehicle newVehicle);
        bool RemoveVehicle(int id);
        Vehicle GetVehicle(int id);
        List<Vehicle> GetAllVehicle();
        int GetFreeSpaces();
        decimal GetBalance();
        int GetVehiclesCount();
        List<Transaction> GetTransactions();
    }
}
