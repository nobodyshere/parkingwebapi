﻿using ParkingLib.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;


namespace ParkingLib
{
    public class ParkingStation : IParkingStation
    {
        private decimal balance;
        private List<Vehicle> transports;
        private Dictionary<Vehicle, Timer> vehicleTimers;
        private Timer logTimer;

        static public List<Transaction> transactions;

        public decimal GetProfitForLastMinute() => transactions.Where(x => x.TransactionTime >= DateTime.Now.AddSeconds(-60)).Sum(x => x.Tax);

        public ParkingStation()
        {
            transports = new List<Vehicle>();
            transactions = new List<Transaction>();
            vehicleTimers = new Dictionary<Vehicle, Timer>();
            logTimer = new Timer(Logger.WriteLog, null, Settings.logPeriod, Settings.logPeriod);
        }

        public List<Transaction> GetTransactionsPerMinute()
        {
            if (transactions.Count > 0)
            {
                List<Transaction> list = new List<Transaction>();
                foreach (var transaction in transactions.Where(x => x.TransactionTime >= DateTime.Now.AddSeconds(-60)))
                {
                    list.Add(transaction);
                }

                return list;
            }
            else
            {
                return null;
            }
        }

        private void GetPayment(object o)
        {
            var vehicle = o as Vehicle;
            decimal tax = GetTaxRate(vehicle);
            Transaction transaction;
            if (tax > vehicle.Balance)
            {
                vehicle.AddFine(tax * Settings.fine, out transaction);
                transactions.Add(transaction);
            }
            else
            {
                vehicle.Pay(tax, out transaction);
                balance += transaction.Tax;
                transactions.Add(transaction);
            }

        }

        public bool AddVehicle(Vehicle newVehicle)
        {
            if (transports.Count >= Settings.maxSpace)
            {
                Console.WriteLine("There are no space on parking");
                return false;
            }
            else
            {
                Timer timer = new Timer(GetPayment, newVehicle, Settings.period, Settings.period);
                vehicleTimers.Add(newVehicle, timer);
                transports.Add(newVehicle);
                return true;
            }
        }

        public bool RemoveVehicle(int id)
        {
            var vehicle = transports.Find(x => x.Id == id);
            if (vehicle != null && vehicle.Fine == 0)
            {
                transports.RemoveAll(x => x.Id == id);
                return true;
            }
            else
            {
                return false;
            }
        }

        public Vehicle GetVehicle(int id) => transports.FirstOrDefault(x => x.Id == id);

        public List<Vehicle> GetAllVehicle()
        {
            if (transports.Count < 1)
            {
                return null;
            }
            else
            {
                return transports;
            }
        }

        public int GetFreeSpaces()
        {
            return Settings.maxSpace - GetVehiclesCount();
        }

        public decimal GetBalance() => balance;

        private decimal GetTaxRate(Vehicle vehicle)
        {
            switch (vehicle.Type.ToString())
            {
                case "Car":
                    return 2m;
                case "Truck":
                    return 5m;
                case "Bus":
                    return 3.5m;
                case "Motorcycle":
                    return 1m;
                default:
                    return 2m;
            }
        }

        public int GetVehiclesCount() => transports.Count;

        public List<Transaction> GetTransactions() => transactions;
    }
}
