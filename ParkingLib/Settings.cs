﻿namespace ParkingLib
{
    public static class Settings
    {
        public static readonly decimal balance = 0;
        public static readonly decimal fine = 2.5m;
        public static readonly int maxSpace = 10;
        public static readonly int period = 5000;
        public static readonly int logPeriod = 60000;
        public static readonly string path = "Transactions.log";
    }
}
