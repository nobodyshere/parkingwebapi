﻿using Newtonsoft.Json;
using System;

namespace ParkingLib
{
    public class Transaction
    {
        [JsonProperty("transportid")]
        public int TransportId { get; }
        [JsonProperty("tax")]
        public decimal Tax { get; }
        [JsonProperty("fine")]
        public decimal Fine { get; }
        [JsonProperty("transactiontime")]
        public DateTime TransactionTime { get; }

        public Transaction(int transportid, decimal tax, decimal fine, DateTime transactiontime)
        {
            TransportId = transportid;
            Tax = tax;
            Fine = fine;
            TransactionTime = transactiontime;
        }

        public override string ToString() => $"Transport Id: {TransportId} \t " +
                $"Payed: {Tax} \t " +
                $"Fine: {Fine} \t" +
                $"Time: {TransactionTime}";

    }
}
