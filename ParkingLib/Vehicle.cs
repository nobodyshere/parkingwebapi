﻿using Newtonsoft.Json;
using System;

namespace ParkingLib
{
    public class Vehicle
    {
        public static int id = 0;
        [JsonProperty("balance")]
        public decimal Balance { get; private set; }
        [JsonProperty("type")]
        public string Type { get; private set; }
        [JsonProperty("fine")]
        public decimal Fine { get; private set; }
        [JsonProperty("id")]
        public int Id { get; set; }

        public Vehicle(decimal balance, VehicleType type)
        {
            Balance = balance;
            Type = type.ToString();
        }

        public bool AddBalance(decimal money)
        {
            if (money > 0)
            {
                Balance += money;
                if (Fine > 0 && Fine < Balance)
                {
                    Balance -= Fine;
                    Fine = 0;
                }
                else if (Fine > 0 && Fine > Balance)
                {
                    Fine -= Balance;
                    Balance = 0;
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        public void Pay(decimal tax, out Transaction transaction)
        {
            Balance -= tax;
            transaction = new Transaction(Id, tax, Fine, DateTime.Now);
        }

        public void AddFine(decimal tax, out Transaction transaction)
        {
            if (Balance > 0)
            {
                tax -= Balance;
                Balance = 0;
            }
            Fine += tax;
            transaction = new Transaction(Id, 0, Fine, DateTime.Now);
        }

        public override string ToString() => $"Vehicle ID: {Id}, Type: {Type.ToString()}, Balance: {Balance}, Fine: {Fine}";
    }
}
