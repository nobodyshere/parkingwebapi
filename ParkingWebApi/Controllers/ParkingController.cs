﻿using Microsoft.AspNetCore.Mvc;
using ParkingLib;
using ParkingLib.Interfaces;
using System.Collections.Generic;

namespace ParkingWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParkingController : ControllerBase
    {
        private IParkingStation _parking;

        public ParkingController(IParkingStation parking)
        {
            _parking = parking;
        }

        //Get parking balance
        [HttpGet("balance")]
        public ActionResult<decimal> GetBalance()
        {
            return _parking.GetBalance();
        }


        [HttpGet("profit")]
        public ActionResult<decimal> GetProfit()
        {
            return _parking.GetProfitForLastMinute();
        }


        [HttpGet("count")]
        public ActionResult<int> GetVehicleCount()
        {
            return _parking.GetVehiclesCount();
        }

        [HttpGet("freespaces")]
        public ActionResult<int> GetFreeSpaces()
        {
            return _parking.GetFreeSpaces();
        }
    }
}