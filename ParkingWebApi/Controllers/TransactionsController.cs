﻿using Microsoft.AspNetCore.Mvc;
using ParkingLib;
using ParkingLib.Interfaces;
using System.Collections.Generic;

namespace ParkingWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionsController : ControllerBase
    {
        private IParkingStation _parking;

        public TransactionsController(IParkingStation parking)
        {
            _parking = parking;
        }

        [HttpGet("{last}")]
        public ActionResult<List<Transaction>> GetTransactions()
        {
            return _parking.GetTransactionsPerMinute();
        }

        [HttpGet]
        public ActionResult<List<Transaction>> GetLogs()
        {
            return _parking.GetTransactions();
        }
    }
}