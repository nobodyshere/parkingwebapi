﻿using Microsoft.AspNetCore.Mvc;
using ParkingLib;
using ParkingLib.Interfaces;
using System.Collections.Generic;

namespace ParkingWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehicleController : ControllerBase
    {
        private IParkingStation _parking;

        public VehicleController(IParkingStation parking)
        {
            _parking = parking;
        }

        [HttpGet]
        public ActionResult<List<Vehicle>> GetVehicle()
        {
            return _parking.GetAllVehicle();
        }

        [HttpGet("{id}")]
        public ActionResult<Vehicle> GetVehicle([FromRoute]int id)
        {
            var vehicle = _parking.GetVehicle(id);
            return vehicle != null ? (ActionResult<Vehicle>)vehicle : NotFound();
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteVehicle([FromRoute]int id)
        {
            bool responce = _parking.RemoveVehicle(id);
            return responce ? Ok() : (IActionResult)NotFound();
        }

        [HttpPost]
        public IActionResult AddVehicle(Vehicle vehicle)
        {
            var responce = _parking.AddVehicle(vehicle);
            return responce ? Ok() : (IActionResult)NotFound();
        }

        [HttpPost("fill/{id}")]
        public IActionResult AddMoney([FromRoute]int id, [FromBody]decimal money)
        {
            Vehicle vehicle = _parking.GetVehicle(id);
            return vehicle != null ? Ok(vehicle.AddBalance(money)) : (IActionResult)NotFound();
        }
    }
}